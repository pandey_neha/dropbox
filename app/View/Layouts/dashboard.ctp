<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Dashboard">
	<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

	<title>Test</title>

	<!-- Bootstrap core CSS -->
	<?php
	echo $this->Html->css('/assets/css/bootstrap.css');
	echo $this->Html->css('/assets/font-awesome/css/font-awesome.css');
	echo $this->Html->css('/assets/css/zabuto_calendar.css');
	echo $this->Html->css('/assets/js/gritter/css/jquery.gritter.css');
	echo $this->Html->css('/assets/lineicons/style.css');
	echo $this->Html->css('/assets/css/style.css');
	echo $this->Html->css('/assets/css/style.css');
	echo $this->Html->css('/assets/css/style-responsive.css'); ?>
	<!--	<link href="assets/css/bootstrap.css" rel="stylesheet">
    -->    <!--external css-->
	<!--	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
        <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">
    -->
	<!-- Custom styles for this template -->
	<!--	<link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/style-responsive.css" rel="stylesheet">
    -->
	<?php echo $this->Html->script('assets/js/chart-master/Chart.js'); ?>
	<!--<script src="assets/js/chart-master/Chart.js"></script>-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<?php echo $this->Html->script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'); ?>
	<?php echo $this->Html->script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'); ?>
<!--
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
	<![endif]-->
</head>

<body>

<section id="container">
	<!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
	<!--header start-->
	<?php echo $this->element('header'); ?>
	<!--header end-->

	<!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
	<!--sidebar start-->
	<?php echo $this->element('sidebar'); ?>

	<!--sidebar end-->

	<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
	<!--main content start-->
	<section id="main-content">
		<section class="wrapper">
			<?php echo $this->fetch('content');; ?>
		</section>
	</section>

	<!--main content end-->
	<!--footer start-->
	<!--	--><?php /*echo $this->element('footer'); */ ?>

	<!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
<?php /*echo $this->Html->script("/assets/js/jquery.js"); */ ?>
<?php echo $this->Html->script("/assets/js/jquery.js"); ?>
<?php echo $this->Html->script("/assets/js/jquery-1.8.3.min.js"); ?>
<?php echo $this->Html->script("/assets/js/bootstrap.min.js"); ?>
<?php echo $this->Html->script("/assets/js/jquery.dcjqaccordion.2.7.js"); ?>
<?php echo $this->Html->script("/assets/js/jquery.scrollTo.min.js"); ?>
<?php echo $this->Html->script("/assets/js/jquery.nicescroll.js"); ?>
<?php echo $this->Html->script("/assets/js/jquery.sparkline.js"); ?>
<!--<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery-1.8.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="assets/js/jquery.sparkline.js"></script>
-->

<!--common script for all pages-->
<?php echo $this->Html->script("/assets/js/common-scripts.js"); ?>
<?php echo $this->Html->script("/assets/js/gritter/js/jquery.gritter.js"); ?>
<?php echo $this->Html->script("/assets/js/gritter-conf.js"); ?>

<!--<script src="assets/js/common-scripts.js"></script>

<script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="assets/js/gritter-conf.js"></script>
-->
<!--script for this page-->
<?php echo $this->Html->script("/assets/js/sparkline-chart.js"); ?>
<?php echo $this->Html->script("/assets/js/zabuto_calendar.js"); ?>

<!--<script src="assets/js/sparkline-chart.js"></script>
<script src="assets/js/zabuto_calendar.js"></script>
-->
<script type="text/javascript">
	$(document).ready(function () {
		var unique_id = $.gritter.add({
			// (string | mandatory) the heading of the notification
			title: 'Welcome to Dashgum!',
			// (string | mandatory) the text inside the notification
			text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
			// (string | optional) the image to display on the left
			image: 'assets/img/ui-sam.jpg',
			// (bool | optional) if you want it to fade out on its own or just sit there
			sticky: true,
			// (int | optional) the time you want it to be alive for before fading out
			time: '',
			// (string | optional) the class name you want to apply to that specific message
			class_name: 'my-sticky-class'
		});

		return false;
	});
</script>

<script type="application/javascript">
	$(document).ready(function () {
		$("#date-popover").popover({html: true, trigger: "manual"});
		$("#date-popover").hide();
		$("#date-popover").click(function (e) {
			$(this).hide();
		});

		$("#my-calendar").zabuto_calendar({
			action: function () {
				return myDateFunction(this.id, false);
			},
			action_nav: function () {
				return myNavFunction(this.id);
			},
			ajax: {
				url: "show_data.php?action=1",
				modal: true
			},
			legend: [
				{type: "text", label: "Special event", badge: "00"},
				{type: "block", label: "Regular event",}
			]
		});
	});


	function myNavFunction(id) {
		$("#date-popover").hide();
		var nav = $("#" + id).data("navigation");
		var to = $("#" + id).data("to");
		console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
	}
</script>


</body>
</html>
