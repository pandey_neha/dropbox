<!DOCTYPE html>
<head>
	<title> demo </title>

	<?php
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');

	?>
</head>
<body>
<div> <?php echo $this->element('header'); ?> </div>
<div>
	<?php echo $this->session->flash(); ?>
	<?php echo $this->fetch('content'); ?>


</div>
<div> <?php echo $this->element('footer'); ?> </div>

</body>
</html>
